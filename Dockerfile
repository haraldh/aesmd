# Import the Intel signing key
FROM ubuntu:focal AS keyring
RUN apt update \
 && apt install --no-install-recommends -y gnupg \
 && rm -rf /var/lib/apt/lists/*
COPY intel-sgx.key /tmp/
RUN apt-key add /tmp/intel-sgx.key


FROM ubuntu:focal

# Set up the Intel repository
RUN apt update \
 && apt install --no-install-recommends -y ca-certificates \
 && rm -rf /var/lib/apt/lists/*
COPY --from=keyring /etc/apt/trusted.gpg /etc/apt/trusted.gpg
COPY intel-sgx.list /etc/apt/sources.list.d/

# Install aesmd
RUN apt update \
 && apt install --no-install-recommends -y sgx-aesm-service libsgx-aesm-pce-plugin libsgx-aesm-ecdsa-plugin \
 && rm -rf /var/lib/apt/lists/*

# Launch aesmd
RUN mkdir /var/run/aesmd
ENV LD_LIBRARY_PATH=/opt/intel/sgx-aesm-service/aesm
CMD ["/opt/intel/sgx-aesm-service/aesm/aesm_service", "--no-daemon"]
