# How to use this container

`podman run --rm -it --device /dev/sgx_enclave -v /var/run/aesmd:/var/run/aesmd registry.gitlab.com/enarx/aesmd`

The above command will run the container interactively (`-it`) on your system.
It will also remove the container image when execution is complete (`--rm`).

During execution, the host directory `/var/run/aesmd` will be mounted into the
container at the location `/var/run/aesmd`. This means that the socket created
by the daemon process at `/var/run/aesmd/aesm.socket` will be visible on the
host at the same path.

Likewise, the `/dev/sgx_enclave` device node is shared to the container so that
`aesmd` can access SGX.

You can tweak these arguments as necessary.
